import createHistory from "history/createBrowserHistory";
import { applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";
import promise from "redux-promise-middleware";
import reducers from "./reducers.js";
import { routerMiddleware } from "react-router-redux";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

/**
 * Set up basic persist configuration
 * Persist makes sure that redux caches the data it stores
 */
const persistConfig = {
  key: "root",
  storage
};

/**
 * Main store configuration for redux
 */

// history create history object with functions and data
// for where we are on the site
const history = createHistory();
// sets initial state of redux
const initialState = {};
// middleware gives us a lot of nifty features in react
const middleware = applyMiddleware(promise(), thunk, routerMiddleware(history));
// create the persisted reducer
const persistedReducer = persistReducer(persistConfig, reducers);

/**
 * Create the reducers store
 */
const store = createStore(
  persistedReducer,
  initialState,
  composeWithDevTools(middleware)
);

// Sets the persistor
let persistor = persistStore(store);

// export all for use in app.js
export { store, history, persistor };
