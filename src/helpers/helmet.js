import React, { Component } from "react";
import { Helmet } from "react-helmet";

export default function CustomHelmet(data) {
  return (
    <Helmet>
      <meta charSet="utf-8" />
      <title>
        {data.yoast_meta.yoast_wpseo_title
          ? data.yoast_meta.yoast_wpseo_title
          : null}
      </title>
      <meta
        name="description"
        content={
          data.yoast_meta.yoast_wpseo_metadesc
            ? data.yoast_meta.yoast_wpseo_metadesc
            : null
        }
      />
      <link
        rel="canonical"
        href={
          data.yoast_meta.yoast_wpseo_canonical
            ? data.yoast_meta.yoast_wpseo_canonical
            : null
        }
      />
      <meta
        property="og:url"
        content={
          data.yoast_meta.yoast_wpseo_canonical
            ? data.yoast_meta.yoast_wpseo_canonical
            : null
        }
      />
      <meta
        property="og:title"
        content={
          data.yoast_meta.yoast_wpseo_title
            ? data.yoast_meta.yoast_wpseo_title
            : null
        }
      />
      <meta
        property="og:description"
        content={
          data.yoast_meta.yoast_wpseo_metadesc
            ? data.yoast_meta.yoast_wpseo_metadesc
            : null
        }
      />
      <meta
        name="twitter:card"
        content={
          data.yoast_meta.yoast_wpseo_metadesc
            ? data.yoast_meta.yoast_wpseo_metadesc
            : null
        }
      />
    </Helmet>
  );
}
