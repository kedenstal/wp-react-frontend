import { combineReducers } from "redux";
import { routerReducer } from "react-router-redux";
import { reducer as formReducer } from "redux-form";
import { postsReducer } from "../components/posts/posts.redux";

/**
 * reducers.js sets all the reducers that are available to us
 * anytime you create a new reducer/action file, they should be imported and placed in here
 */

export default combineReducers({
  routerReducer,
  form: formReducer,
  posts: postsReducer
});
