import React, { Component, Fragment } from "react";
import axios from "axios";
import keys from "../config/keys";
import BounceLoader from "react-spinners/BounceLoader";

class WPFetchPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loaded: false,
      data: false
    };

    this.renderChildren = this.renderChildren.bind(this);
    this.fetchByPage = this.fetchByPage.bind(this);
  }

  componentDidMount() {
    this.fetchByPage(this.props.slug);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.type && this.props.type) {
      if (nextProps.type !== this.props.type) {
        this.setState({ data: false, loaded: false });
        this.fetchByType(nextProps.type);
      }
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.data !== this.state.data) {
      return true;
    } else {
      return false;
    }
  }

  renderChildren() {
    const props = this.props;
    return React.Children.map(this.props.children, child => {
      return React.cloneElement(child, {
        data: this.state.data,
        ...props
      });
    });
  }

  fetchByPage(slug) {
    let url = `${keys.wpApiEndpoint}/pages`;
    let request = axios.get(url).then(
      response => {
        if (response.data && response.data.length) {
          return response.data.map(findPage => {
            if (findPage.slug === slug) {
              this.setState({
                data: findPage,
                loaded: true
              });
              return response;
            } else {
              return null;
            }
          });
        }
      },
      error => {
        console.error(error);
        return null;
      }
    );
  }

  render() {
    if (this.state.loaded) {
      return this.renderChildren();
    } else {
      return <BounceLoader color={"#d8bfd8"} />;
    }
  }
}

export default WPFetchPage;
