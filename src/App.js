import React, { Component } from "react";
import "./helpers/polyfill";
import { Switch, Route } from "react-router-dom";
import { ConnectedRouter as Router } from "react-router-redux";
import { Provider } from "react-redux";
import { store, history, persistor } from "./helpers/store";
import asyncComponent from "./hocs/asyncComponent";
import { PersistGate } from "redux-persist/integration/react";
import ErrorBoundary from "react-error-boundary";
import WPFetchPage from "./hocs/WPFetchPage";

const Frontpage = asyncComponent(() => import("./components/pages/Frontpage"));
const NotFound = asyncComponent(() => import("./components/pages/NotFound"));
const Posts = asyncComponent(() => import("./components/pages/Posts"));

class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="App">
        <Provider store={store}>
          <PersistGate loading={null} persistor={persistor}>
            <Router history={history}>
              <Switch>
                <ErrorBoundary>
                  <Switch>
                    <Route
                      exact
                      path="/"
                      render={props => (
                        <WPFetchPage {...props} slug={"frontpage"}>
                          <Frontpage />
                        </WPFetchPage>
                      )}
                    />
                    <Route
                      exact
                      path="/posts"
                      render={props => (
                        <WPFetchPage {...props} slug={"posts"}>
                          <Posts />
                        </WPFetchPage>
                      )}
                    />
                    <Route path="*" component={NotFound} />
                  </Switch>
                </ErrorBoundary>
              </Switch>
            </Router>
          </PersistGate>
        </Provider>
      </div>
    );
  }
}

export default App;
