import React, { Component } from "react";
import renderHTML from "react-render-html";

class PostItem extends Component {
  constructor(props) {
    super(props);
  }

  renderTestImage() {
    if (this.props.post.acf && this.props.post.acf.test_image) {
      return <img src={this.props.post.acf.test_image.url} />;
    }
  }

  render() {
    console.log(this.props);
    return (
      <div className={"post-item"}>
        <h2>
          <a href={`/posts/${this.props.post.slug}`}>
            {this.props.post.title.rendered}
          </a>
        </h2>
        <div className={"post-content"}>
          {this.renderTestImage()}
          {renderHTML(this.props.post.content.rendered)}
        </div>
      </div>
    );
  }
}

export default PostItem;
