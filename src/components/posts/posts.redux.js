import keys from "../../config/keys";
import { store } from "../../helpers/store";
import axios from "axios";

/** Types
 * Types are used as a way for redux to know what to do with the data it fetches
 */

const FETCH_ALL_POSTS = "FETCH_ALL_POSTS";
const FETCH_SINGLE_POST = "FETCH_SINGLE_POST";

/** Initial State
 * This sets the initial state of the reducer
 */

const INITIAL_STATE = {
  all: {},
  single: {}
};

/** Reducer
 * The reducer sets all data that we can use
 * It works as a regular switch
 * Takes the type that we declared earlier and we use in export functions
 * and inserts correct data into that object in the reducer
 */

export function postsReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case FETCH_ALL_POSTS:
      return { ...state, all: action.payload.data };
    default:
      return state;
  }
}

// Standard constants
const baseUrl = `${keys.wpApiEndpoint}/posts`;
const env = keys.apiEnvironment;

/** Functions are declared here that can be used in components */
export function fetchAllPosts() {
  const url = baseUrl;
  const request = axios.get(url);

  return dispatch => {
    request.then(res => {
      dispatch({
        type: FETCH_ALL_POSTS,
        payload: res
      });
    });
  };
}

export function fetchPost(id) {
  const url = `${baseUrl}/${id}`;
  const request = axios.get(url);

  return dispatch => {
    request.then(res => {
      dispatch({
        type: FETCH_SINGLE_POST,
        payload: res
      });
    });
  };
}
