import React, { Component } from "react";
import { connect } from "react-redux";
import PostItem from "./templates/PostItem";

class PostListing extends Component {
  constructor(props) {
    super(props);
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.all.length !== this.props.all.length) {
      return true;
    }
    return false;
  }

  renderPostListing() {
    return this.props.all.map((post, index) => {
      return <PostItem post={post} key={index} />;
    });
  }

  render() {
    if (this.props.all && this.props.all.length) {
      return <div className={"post_listings"}>{this.renderPostListing()}</div>;
    } else {
      return null;
    }
  }
}

function mapStateToProps({ posts }) {
  const { all } = posts;
  return {
    all
  };
}

export default (PostListing = connect(
  mapStateToProps,
  null
)(PostListing));
