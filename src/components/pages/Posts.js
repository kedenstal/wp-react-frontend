import React, { Component } from "react";
import CustomHelmet from "../../helpers/helmet";
import PostListing from "../posts/PostListing";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { fetchAllPosts } from "../posts/posts.redux";
class Posts extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.fetchAllPosts();
  }

  render() {
    let data;
    if (
      this.props &&
      this.props.data &&
      this.props.all &&
      this.props.all.length
    ) {
      data = this.props.data;
      return (
        <div className={"posts-list"}>
          {CustomHelmet(data)}
          all of our posts listed below.
          <PostListing />
        </div>
      );
    } else {
      return null;
    }
  }
}

function mapStateToProps({ posts }) {
  const { all } = posts;
  return {
    all
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchAllPosts
    },
    dispatch
  );
}

Posts = withRouter(Posts);
export default (Posts = connect(
  mapStateToProps,
  mapDispatchToProps
)(Posts));
