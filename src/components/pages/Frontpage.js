import React, { Component } from "react";
import CustomHelmet from "../../helpers/helmet";

class Frontpage extends Component {
  render() {
    console.log(this.props);
    let data;
    if (this.props && this.props.data) {
      data = this.props.data;
      return (
        <div className={"frontpage"}>
          {CustomHelmet(data)}
          frontpage
          <p>just a blank frontpage</p>
        </div>
      );
    } else {
      return null;
    }
  }
}

export default Frontpage;
