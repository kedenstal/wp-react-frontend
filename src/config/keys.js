const prodKeys = {
  wpApiEndpoint: "http://mysite.com/index.php/wp-json/wp/v2",
  apiEnvironment: "prod"
};

const devKeys = {
  wpApiEndpoint: "http://wp-headless.awave.site/index.php/wp-json/wp/v2",
  apiEnvironment: "dev"
};

const keys = devKeys;

export default keys;
